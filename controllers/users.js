"use strict";
var express=require('express');
var jwt = require('jsonwebtoken');
var request=require('request').defaults({ encoding: null });
var reset_password=require('../utility/reset_password');
var mongoose=require('mongoose');
var body_parser=require('body-parser');
var body_parser_middleware=body_parser.json();
var body_parser_middleware1=body_parser.urlencoded({extended:true});
var router=express.Router();
let userModel=mongoose.model('users');
// Register API for Create New Account !!
router.post("/register",[body_parser_middleware1,body_parser_middleware],(req,resp)=>{
	console.log(req.body);
  req.body._id=Math.random().toString(36).substring(7)+(+new Date());
  let user=new userModel(req.body);
  user.save(function(error,result){
    console.log(error);
    if(!error)
      userModel.populate(result,{path:"childeren",select:"first_name last_name avatar points"},(err,childeren)=>{
        resp.json(childeren);
      })
    else
      resp.json(error);
  });
});

router.post("/login",body_parser_middleware,(req,resp)=>{

  userModel.findOne({$and:[{$or:[{email:{'$regex':`^${req.body.key}$`, '$options' : 'i'}},{phone:req.body.key}]},{password:req.body.password}]},(error,user)=>{
    if(user)
      userModel.populate(user,{path:"childeren.id",select:"first_name last_name avatar points"},(err,childeren)=>{
	console.log(err);
        resp.json(childeren);
      })
    else
      resp.json({login:false});
  })
});
// Perform Login Process!!
router.post("/update",body_parser_middleware,(req,resp)=>{
  //let userModel=mongoose.model('users');
  userModel.update({_id:req.body._id},{"$set":req.body},(err,result)=>{
    resp.json(result);
  })
});

router.post("/social",[body_parser_middleware1,body_parser_middleware],(req,resp)=>{
    userModel.findOne({_id:req.body._id},function(error,users){
      console.log(req.body);
      if(!users){
        request(req.body.avatar,(err,response,body)=>{
	  console.log(err);
	  if(response)
           req.body.avatar = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64')
	  else
	   req.body.avatar="assets/images/avatar.png";
          userModel.findOneOrCreate({_id:req.body._id},req.body,(err,user)=>{
            userModel.populate(user,{path:"childeren.id",select:"first_name last_name avatar points email"},(err,childeren)=>{
              if(err)
                resp.json({error:true});
              else
                resp.json(childeren);
            })
          });
        })
      }else{
        userModel.populate(users,{path:"childeren.id",select:"first_name last_name avatar points email"},(err,childeren)=>{
          if(err)
            resp.json({error:true});
          else
            resp.json(childeren);
        })
      }
    });
})


router.get('/exists/:email',(req,resp)=>{
  userModel.find({email:{'$regex':`^${req.params.email}$`, '$options' : 'i'}},(err,users)=>{
    if(!err){
      if(users.length>0)
        resp.json({exists:true});
      else
        resp.json({exists:false})
    }else{
        resp.json({exists:false})
    }
  })
})






router.get('/get_childern/:id',(req,resp)=>{
  userModel.findOne({_id:req.params.id},{_id:false,childeren:true},(err,parent)=>{
    userModel.populate(parent,{path:"childeren.id",select:"first_name last_name avatar points email"},(err,childeren)=>{
      resp.json(childeren);
    })
  })
})

router.get('/get_points/:user_id',(req,resp)=>{
  userModel.findOne({_id:req.params.user_id},{_id:false,points:true},(err,points)=>{
      resp.json(points);
  })
})

router.get('/phone/:phone/:user_id',(req,resp)=>{
  userModel.findOne({phone:req.params.phone,_id:{$ne:req.params.user_id}},{_id:true},(err,user)=>{
      if(user)
        resp.json({exists:true});
      else
        resp.json({exists:false});
  })
})

router.post("/accounts/",body_parser_middleware,(req,resp)=>{
  var regex = req.body.numbers.map(function (e) { return new RegExp(`.*${e}$`); });
  userModel.find({"$and":[{phone:{$in:regex}},{_id:{$ne:req.body.id}}]},{first_name:true,last_name:true,phone:true},(err,users)=>{
    console.log(users);
    resp.json(users);
  })
})


router.get('/exists_profile/:email/:id',(req,resp)=>{
 userModel.find({email:{'$regex':`^${req.params.email}$`, '$options' : 'i'},_id:{$ne:req.params.id}},(err,users)=>{
    if(!err){
      if(users.length>0)
        resp.json({exists:true});
      else
        resp.json({exists:false})
    }else{
        resp.json({exists:false})
    }
  })
})

router.get("/token/:user_id/:token",(req,resp)=>{
  userModel.update({_id:req.params.user_id},{"$set":{dev_token:req.params.token}},(err,rs)=>{
    resp.json({ok:1})
  })
});


router.get("/remove_token/:user_id",(req,resp)=>{
  userModel.update({_id:req.params.user_id},{"$set":{dev_token:''}},(err,rs)=>{
    resp.json({ok:1})
  })
});

router.get("/delete/:user_id",(req,resp)=>{
  userModel.remove({_id:req.params.user_id},(err,rs)=>{
    resp.json(rs)
  })
});

router.get("/reset_password/:email",(req,resp)=>{
  var token=jwt.sign({email: req.params.email}, '$@#@#$', { expiresIn: '1h' });
  reset_password(req.params.email,token);
  userModel.find({email:req.params.email},{_id:true},(err,user)=>{
	console.log(err);
    resp.json(user);
  })
});

module.exports=router;
