"use strict";
var express=require('express');
var notifications=require('../utility/notification');
var jwt = require('jsonwebtoken');
var body_parser=require('body-parser');
var body_parser_middleware1=body_parser.urlencoded({extended:true});
var router=express.Router();
var mongoose=require('mongoose');
var multer=require('multer');
var upload = multer({ storage: multer.memoryStorage({}) })

let userModel=mongoose.model('users');
let advertModel=mongoose.model('adverts');

router.use((req,resp,next)=>{
  resp.locals={
    active:"advert"
  }
  next();
})

router.get("/send",(req,resp)=>{
  resp.render("notification/send",{title:"Send Notification",active:"notification"});
});

router.post("/send",body_parser_middleware1,(req,resp)=>{
  let query={}
  if(req.body.email){
    query.email=req.body.email;
  }
  userModel.find(query,{dev_token:true,_id:false},(err,users)=>{
      users=users.map((user)=>{return user.dev_token})
      notifications.send(req.body.title,req.body.msg,users);
      resp.redirect("/portal/send")
  })
})
router.get("/advert",(req,resp)=>{
  advertModel.find({},(err,adverts)=>{
    resp.render("advert/list",{title:"List Adverts",adverts:adverts});
  })
})

router.get("/advert/add",(req,resp)=>{
  resp.render("advert/add",{title:"Add Advert"});
})

router.get("/advert/delete/:id",(req,resp)=>{
  advertModel.remove({_id:req.params.id},(err,adverts)=>{
    if(err)
      resp.json(err);
    else
    resp.redirect("/portal/advert");
  });
})

router.post("/advert/add",upload.single('advert'),(req,resp)=>{
  var raw = req.file.buffer.toString('base64');
  var advert=new advertModel({data:raw,advert_link:req.body.advert_link});
  advert.save((err,data)=>{
      if(err)
        resp.json(err);
      else
      resp.redirect("/portal/advert");
  })
})


router.get("/adverts_json",(req,resp)=>{
  advertModel.find({},{data:true,advert_link:true,_id:false},(err,adverts)=>{
    if(err)
      resp.json(err);
    else
    resp.json(adverts);
  });
})

router.get("/export",(req,resp)=>{
  var json2csv = require('json2csv');
  var fields = ['first_name', 'last_name', 'phone','email','gender','type','country_code','points','dob_day','dob_month','dob_year'];
  userModel.find({},{first_name:true,last_name:true,phone:true,email:true},(err,users)=>{
    var result = json2csv({ data: users, fields: fields });
    resp.setHeader('Content-disposition', 'attachment; filename=users.csv');
    resp.set('Content-Type', 'text/csv');
    resp.send(result);
  })
})

router.get("/privacy",(req,resp)=>{
  resp.render('privacy',{title:"Privacy Policy",active:"privacy"});
})

router.get("/reset_password/:token",(req,resp)=>{
  jwt.verify(req.params.token, "$@#@#$", (err,decode)=>{
    if(err)
      resp.json("invalid token");
    else
      resp.render('reset_password',{email:decode.email});
  });
});

router.post("/reset_password",body_parser_middleware1,(req,resp)=>{
  if(req.body.password == '' || req.body.c_password == '')
    resp.send("required password missing");
  else if(req.body.password != req.body.c_password)
    resp.send("password mismatch!!");
  else
    userModel.findOneAndUpdate({email:{'$regex':`^${req.body.email}$`, '$options' : 'i'}},{"$set":{password:req.body.password}},(err,result)=>{
	console.log(err);
	if(result){

        notifications.send("You Password Resetted","You Password Resetted",result.dev_token,'');
        resp.send("Password reset successfully");
      }else{
        resp.send("Something error , try again later");
      }
    });
});

router.get("/country",(req,resp)=>{
  var countries= require('country-data').callingCountries
  countries=countries.all.map(coutry=>{
    return {code:coutry.countryCallingCodes[0],emoji:coutry.emoji,name:coutry.ioc}
  });
  countries=countries.filter(coutry=>{return coutry.name!="" && coutry.emoji!=""});
  resp.json(countries);
})


router.post("/login",body_parser_middleware1,(req,resp)=>{

  if(req.body.username=='admin' && req.body.password=='!@#%*!!'){
    req.session.username='admin';
    req.session.password='!@#%*!!';
    resp.redirect('/portal/send');
  }else
    resp.redirect('/portal/login');
})

module.exports=router;
