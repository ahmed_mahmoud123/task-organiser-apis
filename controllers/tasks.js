"use strict";
var express=require('express');
var mongoose=require('mongoose');
var body_parser=require('body-parser');
var notifications=require('../utility/notification');

var body_parser_middleware=body_parser.json();
var router=express.Router();
let TaskModel=mongoose.model('tasks');
let UserModel=mongoose.model('users');

router.post("/create",body_parser_middleware,(req,resp)=>{
  UserModel.findOne({_id:req.body.child},{dev_token:true,_id:false},(err,child)=>{
    if(child){
      let task=new TaskModel(req.body);
      task.save((err,result)=>{
        if(!err){
          UserModel.findOne({_id:req.body.parent},{first_name:true,last_name:true,_id:false},(err,p)=>{
            notifications.send(`New task from ${p.first_name} ${p.last_name}`,req.body.title,child.dev_token,result._id);
            resp.json(result);
          });
        }else{
          resp.json(err);
        }
      })
    }
  })
})

router.post("/update",body_parser_middleware,(req,resp)=>{
  console.log(req.body);
  TaskModel.findOne({_id:req.body._id},{child:true,title:true},(err,task)=>{
    UserModel.populate(task,{path:"child"},(err,task)=>{
      if(task)
        notifications.send(task.title,"colleage update task",task.child.dev_token,task._id);
    })
  })
  TaskModel.update({_id:req.body._id},{"$set":req.body},(err,result)=>{
    mongoose.model('recurringTasks').remove({task_id:req.body._id},(err,res)=>{})
    resp.json({ok:1})
  })
})

router.get("/get/:child/:parent/:status/:yearMonth",(req,resp)=>{
 console.log('here');
  if(req.params.child==req.params.parent){
    TaskModel.find({parent:req.params.child,child:req.params.child,status:req.params.status,yearMonth:req.params.yearMonth},{title:true,child:true,parent:true,waiting:true,done:true,lapsed:true,deleted:true}).sort({datetime:-1}).exec(function(err,tasks){
     console.log(tasks); 
     UserModel.populate(tasks,{path:"parent",select:"avatar"},(err,tasks)=>{
        resp.json(tasks);
      })
    })
  }else {
    TaskModel.find({$or:[
      {$and:[{child:req.params.child},{parent:req.params.parent}]},
      {$and:[{child:req.params.parent},{parent:req.params.child}]}
    ],status:req.params.status,yearMonth:req.params.yearMonth},{title:true,child:true,parent:true,waiting:true,done:true,lapsed:true,deleted:true}).sort({datetime:-1}).exec(function(err,tasks){
      UserModel.populate(tasks,{path:"parent",select:"avatar"},(err,tasks)=>{
        resp.json(tasks);
      })
    })
  }
});

router.get("/details/:task_id",(req,resp)=>{
  TaskModel.findOne({_id:req.params.task_id},(err,task)=>{
    UserModel.populate(task,{path:"parent",select:"first_name last_name avatar"},(err,task)=>{
      resp.json(task);
    })
  })
});

router.get("/done/:child_id/:task_id/:child_name/:task",(req,resp)=>{
  console.log(req.params.child_id);
  TaskModel.update({_id:req.params.task_id},{"$set":{waiting:true}},(err,result)=>{
    UserModel.findOne({"childeren.id":req.params.child_id},{dev_token:true,_id:false},(err,parent)=>{
      console.log(parent);
      if(parent)
        notifications.send(req.params.child_name+" marked a task as done",req.params.task,parent.dev_token,req.params.task_id);
      resp.json(result);
    })
  })
});

router.get("/not_complete/:task_id/:child_id/:parent_name/:title",(req,resp)=>{
  TaskModel.update({_id:req.params.task_id},{"$set":{waiting:false}},(err,result)=>{
    UserModel.findOne({_id:req.params.child_id},{dev_token:true,_id:false},(err,parent)=>{
      if(parent)
        notifications.send(req.params.title,"completion disapproved",parent.dev_token,req.params.task_id);
      resp.json(result);
    })
  })
})


router.get("/approve/:task_id/:points/:user_id/:task",(req,resp)=>{
  TaskModel.update({_id:req.params.task_id},{"$set":{status:"closed",done:true,waiting:false}},(err,result)=>{
    UserModel.update({_id:req.params.user_id},{"$inc":{points:req.params.points}},(err,user)=>{
      UserModel.findOne({_id:req.params.user_id},{dev_token:true},(err,token)=>{
        if(token)
        notifications.send(req.params.task,"completion approved",token.dev_token,req.params.task_id);
        resp.json(result);
      });
    })
  })
});

router.post("/add_note",body_parser_middleware,(req,resp)=>{
  TaskModel.update({_id:req.body.task_id},{"$push":{notes:{txt:req.body.note,user_id:req.body.user_id}}},(err,result)=>{
      resp.json({ok:1})
  });
})

router.get("/delete/:task_id",(req,resp)=>{
  TaskModel.update({_id:req.params.task_id},{"$set":{deleted:true,waiting:false,status:'closed'}},(err,result)=>{
    mongoose.model('recurringTasks').remove({task_id:req.params.task_id},(err,res)=>{})
      resp.json({ok:1})
  });
})

router.post("/recurring",body_parser_middleware,(req,resp)=>{
  let id=req.body._id;
  let RecurringTasks=mongoose.model('recurringTasks');
  if(req.body._id){
    req.body.task_id=req.body._id;
    TaskModel.update({_id:req.body._id},{"$set":req.body},(err,res)=>{});
    delete req.body._id;
    RecurringTasks.update({task_id:id+""},{"$set":req.body},(err,res)=>{
      resp.json(res);
    });
  }else{
    var task=new TaskModel(req.body);
    task.save((err,result)=>{
      req.body.task_id=result._id;
      let recurringTasks=new RecurringTasks(req.body);
      recurringTasks.save();
      resp.json(result);
    })
  }
})

router.get('/get_task_by_parent/:parent/:child',(req,resp)=>{
  TaskModel.find({child:req.params.child,parent:req.params.parent}).sort({_id:-1}).limit(1).exec((err,task)=>{
    resp.json(task);
  });
});
module.exports=router;
