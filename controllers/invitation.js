"use strict";
var express=require('express');
var mongoose=require('mongoose');
var notifications=require('../utility/notification');
var router=express.Router();
let invitationModel=mongoose.model('invitations');
let userModel=mongoose.model('users');

router.get("/get/:child_id",(req,resp)=>{
  invitationModel.find({to:req.params.child_id},(err,invitations)=>{
    userModel.populate(invitations,{path:"from",select:"first_name last_name avatar"},(err,invitations)=>{
      resp.json(invitations)
    })
  })
});


router.get('/add/:parent/:child/:parent_id/:relation/:username',(req,resp)=>{
  userModel.findOne({"$or":[{email:{'$regex':`^${req.params.child}$`, '$options' : 'i'}},{_id:req.params.child}]},(err,child)=>{
    if(child){
      invitationModel.findOne({from:req.params.parent_id,to:child._id},(err,invitation)=>{
        if(invitation){
          resp.json({ok:-1});
        }else{
              notifications.send("Add request","from "+req.params.username,child.dev_token);
              let invitation=new invitationModel({
                from:req.params.parent_id,
                to:child._id,
                relation:req.params.relation
              });
              invitation.save((err,invitation)=>{
                resp.json(invitation);
              })
        }
      })

    }
  })
})

router.get('/perform/:id/:code',(req,resp)=>{
  invitationModel.findOne({_id:req.params.id},(err,invitation)=>{
    if(req.params.code == 1){
      userModel.update({_id:invitation.from},{"$addToSet":{childeren:{id:invitation.to,type:invitation.relation}}},(err,result)=>{})
      userModel.update({_id:invitation.to},{"$addToSet":{childeren:{id:invitation.from,type:invitation.relation}}},(err,result)=>{})
    }
    userModel.populate(invitation,{path:"from to",select:'first_name last_name dev_token'},(err,inv)=>{
      let msg=req.params.code==1?"accepted":"declined";
      notifications.send(inv.to.first_name+" "+inv.to.last_name,msg+" your add request",inv.from.dev_token);
    })
    invitationModel.remove({_id:req.params.id},(err,result)=>{});
    resp.json({ok:1})
  })
})

module.exports=router;
