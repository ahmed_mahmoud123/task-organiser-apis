var mongoose=require('mongoose');

var Schema=mongoose.Schema;

var tasks=new Schema({
  title:Schema.Types.Mixed,
  desc:{
    type:Schema.Types.Mixed,
    default:""
  },
  parent:{
    type:String,
    ref:"users"
  },
  child:{
    type:String,
    ref:"users"
  },
  points:{
    type:Number,
    default:0
  },
  notes:[{
    txt:Schema.Types.Mixed,
    user_id:String
  }],
  status:{
    type:String,
    enum:['open','waiting','closed'],
    default:'open'
  },
  deadline:{
    type:Date
  },
  waiting:{
    type:Boolean,
    default:false
  },
  done:{
    type:Boolean,
    default:false
  },
  next_time:{
    type:Number
  },
  lapsed:{
    type:Boolean,
    default:false
  },
  deleted:{
    type:Boolean,
    default:false
  },
  yearMonth:String
});
mongoose.model("tasks",tasks);
