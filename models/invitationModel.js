var mongoose=require('mongoose');

var Schema=mongoose.Schema;

var invitation=new Schema({
  from:String,
  to:String,
  relation:{
    type:String,
    default:"personal"
  }
});


mongoose.model("invitations",invitation);
