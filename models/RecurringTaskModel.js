var mongoose=require('mongoose');

var Schema=mongoose.Schema;

var recurringTasks=new Schema({

  task_id:{
    type:String,
    ref:"tasks"
  },
  parent:{
    type:String,
    ref:"users"
  },
  child:{
    type:String,
    ref:"users"
  },
  start_recurring:{
      type:Number
  },
  end_recurring:{
    type:Number
  },
  next_time:{
    type:Number
  },
  period:{
    type:Number
  },
  yearMonth:String
});
mongoose.model("recurringTasks",recurringTasks);
