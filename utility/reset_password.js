var mailer = require("nodemailer");
// Use Smtp Protocol to send Email
var smtpTransport = mailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user: 'tasksorganiser@gmail.com',
    pass: 'ahmedkesha123'
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
});


module.exports=(email,token)=>{
  var mail = {
    from: "Task Organiser App <support@task-organiser.com>",
    to: email,
    subject: "Rest Password Task Organiser",
    text: "Rest Password",
    html: `<b><a href='http://www.taskorganiser.co.uk:3000/portal/reset_password/${token}'>Reset Password Link</a> Active for only 1 Hour</b>`
  };
  smtpTransport.sendMail(mail, function(error, response){
    smtpTransport.close();
  });
}
