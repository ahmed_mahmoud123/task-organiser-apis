var notifications=require('./notification');
module.exports=(server)=>{
  var mongoose=require('mongoose');
  let UserModel=mongoose.model('users');
  var io=require('socket.io')(server,{'pingInterval': 2000, 'pingTimeout': 5000});
  var users={};
  io.on("connection",client=>{
    client.on('new_event',msg=>{
     switch(msg.event_name){
        case 'new_invitation':
          UserModel.findOne({$or:[{email:msg.to},{_id:msg.to}]},{_id:true},(err,user)=>{
            if(users[user._id])
              users[user._id].emit('new_event',msg)
          });
        break;
        case 'logout':
            if(users[msg.user])
              delete users[msg.user];
        break;
        case 'task_chat':
          UserModel.findOne({$or:[{email:msg.to},{_id:msg.to}]},{_id:true,dev_token:true},(err,user)=>{
            if(user){
              client.emit('new_event',msg)
              notifications.send(`New chat from ${msg.username}`,msg.task,user.dev_token,'');
            }
          });
        default:
          if(users[msg.to])
            users[msg.to].emit('new_event',msg);
          //msg.username="You";
          //client.emit('new_event',msg)
      }
    });

    client.on("join",name=>{
      console.log("new user join"+name);
      client.user_id=name;
      users[name]=client;
    });


    client.on("logout",name=>{
      console.log("Logout"+name);
      delete users[name];
    });

    client.on('disconnect',()=>{
      console.log("Logged Out "+client.user_id);
      delete users[client.user_id];
    })
  });
}
