"use strict"
var schedule = require('node-schedule');
var mongoose=require('mongoose');
let TaskModel=mongoose.model('tasks');
let UserModel=mongoose.model('users');
let RecurringTasks=mongoose.model('recurringTasks');


var notifications=require('../utility/notification');

module.exports.deadlineTasks=()=>{
  var j = schedule.scheduleJob('*/1 * * * *', function(){
    console.log("============Deadline running==========");
    TaskModel.find({$and:[{status:"open"},{waiting:false},{deadline:{$exists:true}}]},(err,tasks)=>{
      UserModel.populate(tasks,{path:"parent child",select:"dev_token"},(err,tasks)=>{
        let current_time=new Date();
        tasks.forEach((task)=>{
          if(current_time>task.deadline){
            TaskModel.update({_id:task._id},{"$set":{status:"closed",waiting:false,lapsed:true}},(err,result)=>{
                notifications.send("Time lapsed",task.title,task.parent.dev_token,task._id);
                notifications.send("Time lapsed",task.title,task.child.dev_token,task._id);
            })
          }
        })

      })
    })
  })
}

module.exports.recurringTasks=()=>{
  var j = schedule.scheduleJob('*/1 * * * *', function(){
    console.log("============Recurring running==========");
    let current_time=+new Date();
    RecurringTasks.find({$and:[{next_time:{$lte:current_time}},{$or:[{end_recurring:{$gte:current_time}},{end_recurring:-1}]},{start_recurring:{$lte:current_time}}]},(err,tasks)=>{
      TaskModel.populate(tasks,{path:"task_id"},(err,tasks)=>{
        UserModel.populate(tasks,{path:"child parent",select:"dev_token"},(err,tasks)=>{
        let new_task={};
        tasks.forEach((task)=>{
          if(task.task_id){
            if(task.status=="open"){
              TaskModel.update({_id:task.task_id._id},{"$set":{status:"closed",waiting:false,lapsed:true}},(err,result)=>{
                  notifications.send("Time lapsed",task.task_id.title,task.parent.dev_token,task.task_id._id);
                  notifications.send("Time lapsed",task.task_id.title,task.child.dev_token,task.task_id._id);
              })
            }
            new_task={
              "title" : task.task_id.title,
              "desc" : task.task_id.desc,
              "child" : task.task_id.child,
              "parent" : task.task_id.parent,
              "lapsed" : false,
              "done" : false,
              "waiting" : false,
              "status" : "open",
              "notes" : task.task_id.notes,
              "points" : task.task_id.points,
              "next_time" : +new Date() + task.period
            }
            notifications.send("New Recurring Task",task.task_id.title,task.parent.dev_token,task.task_id._id);
            notifications.send("New Recurring Task",task.task_id.title,task.child.dev_token,task.task_id._id);

            new_task=new TaskModel(new_task);
            new_task.save((err,result)=>{
              let recurring={
                "task_id":result._id,
                "period" : task.period,
                "end_recurring" : task.end_recurring,
                "start_recurring" : task.start_recurring,
                "next_time" : +new Date() + task.period
              }
              RecurringTasks.update({_id:task._id},{"$set":recurring},(err,rs)=>{})
            });
          }
        })
      })
    })
    })
  })
}
