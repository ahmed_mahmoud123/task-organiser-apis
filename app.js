var express=require('express');
var app=express();
var session=require('express-session');
var mongoose=require('mongoose');
var http=require('http');
var server=http.createServer(app);

var fs=require('fs');
fs.readdirSync(__dirname+"/models").forEach(function(file){
  require(__dirname+"/models/"+file);
});
require('./utility/realtime')(server);

var job=require('./utility/cronJob');
job.deadlineTasks();
job.recurringTasks();
var usersController=require('./controllers/users');
var portalController=require('./controllers/portal');
var invitationsController=require('./controllers/invitation');
var tasksController=require('./controllers/tasks');

var config=require('./env.json')['development'];
mongoose.connect(config.MONGODB);
app.use(express.static("public"));

app.set('port', (process.env.PORT || 3000));
app.set('view engine', "ejs");

app.use(session({
  secret:'@!$@#%$%^#$'
}));

app.use((req,resp,next)=>{
  if(req.url.indexOf('portal') >=0 ){
    if((req.url.indexOf('country') >=0 )|| (req.url.indexOf('adverts_json') >=0 ) || (req.url.indexOf('reset_password') >=0 ) || (req.method=='POST' && req.url =='/portal/login')){
      next();
    }else if((req.session.username =='admin' && req.session.password == '!@#%*!!'))
      next();
    else
      resp.render('login');
  }else
    next();
});
app.use(function (req, res, next) {
    res.locals={
      port:app.get("port")
    }
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    //res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.use("/users",usersController);
app.use("/invitations",invitationsController);
app.use("/portal",portalController);
app.use("/tasks",tasksController);
app.get("/",(req,resp)=>{
  resp.redirect('/portal/send');
});
server.listen(app.get('port'));
